use regex::Regex;
use types::TpResult;
use types::TpError::{self, Syntax, Underflow};
use types::{false_type, float_type, hash_map_type, int_type, list_type, nil_type, strn_type,
            sym_type, true_type, TpVal, vector_type};
use utils::unescape_str;

static RE: &'static str =
    r###"[\s,]*(~@|[\[\]{}()'`~^@]|"(?:\\.|[^\\"])*"|;.*|[^\s\[\]{}('"`,;)]*)"###;
lazy_static! {
    static ref MALREGEX: Regex = Regex::new(RE).unwrap();
    static ref FLOAT: Regex = Regex::new(r"^-?[0-9]+(\.[0-9]+)?([eE]-?[0-9]+)?$").unwrap();
    static ref INT: Regex = Regex::new(r"^-?[0-9]+$").unwrap();
    static ref STRN: Regex = Regex::new(r#"^".*"$"#).unwrap();
}

struct Reader<'a> {
    tokens: Vec<&'a str>,
    position: usize,
}

impl<'a> Reader<'a> {
    fn new(tokens: Vec<&str>) -> Reader {
        Reader {
            tokens: tokens,
            position: 0,
        }
    }
}

impl<'a> Reader<'a> {
    fn peek(&self) -> Option<&'a str> {
        if let Some(tok) = self.tokens.get(self.position) {
            Some(tok)
        } else {
            None
        }
    }
}

impl<'a> Iterator for Reader<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<&'a str> {
        if let Some(tok) = self.tokens.get(self.position) {
            self.position += 1;
            Some(tok)
        } else {
            None
        }
    }
}

fn tokenize(str: &str) -> Vec<&str> {
    let mut results = Vec::new();

    for cap in MALREGEX.captures_iter(str) {
        let group = cap.at(1).unwrap_or("");
        if group == "" {
            break;
        }
        if group.starts_with(';') {
            continue;
        }
        results.push(group);
    }

    results
}

fn read_atom(rdr: &mut Reader) -> TpResult {
    match rdr.next() {
        Some(tok) => {
            if INT.is_match(tok) {
                let num = try!(tok.parse());
                Ok(int_type(num))
            } else if FLOAT.is_match(tok) {
                let num = try!(tok.parse::<f64>());
                Ok(float_type(num))
            } else if STRN.is_match(tok) {
                let mut tok_str = tok.to_owned();
                let trimmed: String = tok_str.drain(1..tok.len() - 1).collect();
                Ok(strn_type(unescape_str(&trimmed)))
            } else if tok.starts_with(':') {
                let (_, rest) = tok.split_at(1);
                Ok(strn_type(format!("\u{29e}{}", rest)))
            } else if tok == "nil" {
                Ok(nil_type())
            } else if tok == "false" {
                Ok(false_type())
            } else if tok == "true" {
                Ok(true_type())
            } else {
                Ok(sym_type(tok.to_owned()))
            }
        }
        None => Err(Underflow),
    }
}

fn read_seq(rdr: &mut Reader, start: &str, end: &str) -> Result<Vec<TpVal>, TpError> {
    let n_tok = rdr.next();
    if n_tok.is_none() {
        Err(Underflow)
    } else if n_tok == Some(start) {
        let mut ast_vec: Vec<TpVal> = Vec::new();
        loop {
            let p_tok = rdr.peek();
            if p_tok.is_none() {
                return Err(Syntax(format!("expected '{}', got EOF", end)));
            }
            if p_tok == Some(end) {
                break;
            }

            match read_form(rdr) {
                Ok(mv) => ast_vec.push(mv),
                Err(e) => return Err(e),
            }
        }
        rdr.next();
        Ok(ast_vec)
    } else {
        Err(Syntax(format!("expected '{}'", start)))
    }
}

fn read_list_type(rdr: &mut Reader) -> TpResult {
    match read_seq(rdr, "(", ")") {
        Ok(seq) => Ok(list_type(seq)),
        Err(es) => Err(es),
    }
}

fn read_vector_type(rdr: &mut Reader) -> TpResult {
    match read_seq(rdr, "[", "]") {
        Ok(seq) => Ok(vector_type(seq)),
        Err(es) => Err(es),
    }
}

fn read_hash_map_type(rdr: &mut Reader) -> TpResult {
    match read_seq(rdr, "{", "}") {
        Ok(seq) => hash_map_type(seq),
        Err(es) => Err(es),
    }
}

fn read_form(rdr: &mut Reader) -> TpResult {
    match rdr.peek() {
        Some(tok) => {
            match tok {
                "'" => {
                    let _ = rdr.next();
                    match read_form(rdr) {
                        Ok(f) => Ok(list_type(vec![sym_type("quote".to_owned()), f])),
                        Err(e) => Err(e),
                    }
                }
                "`" => {
                    let _ = rdr.next();
                    match read_form(rdr) {
                        Ok(f) => Ok(list_type(vec![sym_type("quasiquote".to_owned()), f])),
                        Err(e) => Err(e),
                    }
                }
                "~" => {
                    let _ = rdr.next();
                    match read_form(rdr) {
                        Ok(f) => Ok(list_type(vec![sym_type("unquote".to_owned()), f])),
                        Err(e) => Err(e),
                    }
                }
                "~@" => {
                    let _ = rdr.next();
                    match read_form(rdr) {
                        Ok(f) => Ok(list_type(vec![sym_type("splice-unquote".to_owned()), f])),
                        Err(e) => Err(e),
                    }
                }
                "@" => {
                    let _ = rdr.next();
                    match read_form(rdr) {
                        Ok(f) => Ok(list_type(vec![sym_type("deref".to_owned()), f])),
                        Err(e) => Err(e),
                    }
                }
                "(" => read_list_type(rdr),
                ")" => Err(Syntax("Unexpected ')'".to_owned())),
                "[" => read_vector_type(rdr),
                "]" => Err(Syntax("Unexpected ']'".to_owned())),
                "{" => read_hash_map_type(rdr),
                "}" => Err(Syntax("Unexpected '}'".to_owned())),
                _ => read_atom(rdr),
            }
        }
        None => Ok(nil_type()),
    }
}

pub fn read_str(str: &str) -> TpResult {
    let tokens = tokenize(str);
    let mut rdr = Reader::new(tokens);
    let res = read_form(&mut rdr);
    if rdr.position == rdr.tokens.len() {
        res
    } else {
        let mut out = String::new();
        for idx in rdr.position..rdr.tokens.len() {
            if let Some(tok) = rdr.tokens.get(idx) {
                out.push_str(tok);
            }
            out.push(' ');
        }
        Err(Syntax(format!("Umatched tokens: {}", out.trim_right())))
    }
}
